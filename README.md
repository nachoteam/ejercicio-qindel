# Ejercicio Qindel

Ejercicio Qindel para la gestion por API rest de lineas de documentos


# Instalacion

El primer paso para poder desplegar satisfactoriamente el proyecto es descargarse el codigo.

Basta con ejecutar git clone sobre este proyecto.

Una vez obtenido el codigo, solo es necesario ejecutarlo con maven (mvn spring-boot:run) y este se desplegara automaticamente en el puerto 8080.

Dado que el proyecto cuenta con la herramienta maven como gestor de dependencias, este automaticamente descargara las bibliotecas necesarias en el proyecto.

En cuanto a la base de datos y persistencia, se ha elegido una base de datos en memoria, en concreto h2, de tal manera que se pueda probar el proyecto rapidamente sin necesidad de configurar nada ni tener que crear el schema.

Una vez desplegado, hay dos rutas importantes.

Swagger: localhost:8080 -> esta interfaz permitira probar facilmente los diferentes endpoints del proyecto, asi como aportar documentación.

base de datos: localhost:8080/h2-console -> esta interfaz permitira conectarse a la base de datos de h2 para poder ver lo que hay almacenado.

- url -> jdbc:h2:mem:filesDB
- usuario -> sa
- no tiene contraseña

En caso de buscar test RestAssured, estan aqui: https://bitbucket.org/nachoteam/testassuredejercicioqindel/src/master/