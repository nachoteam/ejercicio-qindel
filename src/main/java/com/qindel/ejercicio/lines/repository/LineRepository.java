package com.qindel.ejercicio.lines.repository;

import com.qindel.ejercicio.lines.models.repository.Line;
import com.qindel.ejercicio.lines.models.repository.LineIdentity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LineRepository extends JpaRepository <Line, LineIdentity> {

    List<Line> findByLineIdentityFileIdOrderByLineIdentityLineNumberAsc(Integer fileId);
    Line findByLineIdentityFileIdAndLineIdentityLineNumber(Integer fileId, Integer lineNumber);
    int countByLineIdentityFileId(Integer fileId);
    List<Line> findAllByLineIdentityFileIdInAndLineTextContains(List<Integer> fileId, String lineText);

    @Modifying
    @Query(value = "UPDATE LINE SET LINE_NUMBER = LINE_NUMBER + 1 WHERE FILE_ID = :fileId AND LINE_NUMBER >= :lineNumber", nativeQuery = true)
    void updateLinesBeforeInsert(@Param("fileId") Integer fileId, @Param("lineNumber") Integer lineNumber);

    @Modifying
    @Query(value = "UPDATE LINE SET LINE_NUMBER = LINE_NUMBER - 1 WHERE FILE_ID = :fileId AND LINE_NUMBER >= :lineNumber", nativeQuery = true)
    void updateLinesAfterDelete(@Param("fileId") Integer fileId, @Param("lineNumber") Integer lineNumber);
}
