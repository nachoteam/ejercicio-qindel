package com.qindel.ejercicio.lines.controller;

import com.qindel.ejercicio.lines.models.controller.Line;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;


@Api(value = "Line")
public interface LineApiDocumentation {

    // POST METHOD
    @ApiOperation(value = "Line", notes = "Endpoint to insert a line, if there is no line number, it will be appended to the end", tags="edit line")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "internal server error") })
    Integer createNewLineInFile(Line line);

    @ApiOperation(value = "Line", notes = "Endpoint to insert a line", tags="edit line")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No content"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "internal server error") })
    void modifyLineInFile( Line line);

    // DELETE METHOD
    @ApiOperation(value = "Line", notes = "Endpoint to delete a line", tags="edit line")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No content"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "internal server error") })
    void deleteLineInFile( Integer fileId, Integer lineNumber);

    @ApiOperation(httpMethod = "GET", value = "Line", notes = "Endpoint to get a line", response = Line.class, tags="search line")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK", response = Integer.class),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "internal server error") })
    Line getLine(Integer file, Integer lineNumber);


    @ApiOperation(httpMethod = "GET", value = "Line", notes = "Endpoint to filter lines in documents by a certain word", response = List.class, tags="search line")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK", response = List.class),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "internal server error") })
    public List<Line> searchLinesMatchingWord(List<Integer> fileList, String word);

}
