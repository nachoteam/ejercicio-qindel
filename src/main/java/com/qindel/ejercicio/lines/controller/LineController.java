package com.qindel.ejercicio.lines.controller;

import com.qindel.ejercicio.lines.models.controller.Line;
import com.qindel.ejercicio.lines.models.mapper.LineControllerRepositoryMapper;
import com.qindel.ejercicio.lines.service.LineService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/line")
public class LineController implements LineApiDocumentation{

    private LineService lineService;

    public LineController( LineService lineService){
        this.lineService = lineService;
    }

    // POST METHOD
    @PostMapping(path = "/create",
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public Integer createNewLineInFile(@Valid @NotNull @RequestBody Line line){
        return lineService.insertLineInDocument(LineControllerRepositoryMapper.lineControllerToLineRepository(line));
    }

    // teoricamente deberia tener un path variable
    // en este caso va incluido en el body
    @PutMapping(path = "/modify",
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void modifyLineInFile(@Valid @NotNull @RequestBody Line line){
        lineService.updateLineInDocument(LineControllerRepositoryMapper.lineControllerToLineRepository(line));
    }

    // DELETE METHOD
    @DeleteMapping(path = "/{file}/{line}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteLineInFile(@PathVariable(value = "file") Integer fileId, @PathVariable(value = "line") Integer lineNumber){
        lineService.deleteLineInDocument(fileId, lineNumber);
    }

    @GetMapping(path = "/{file}/{line}",produces = {MediaType.APPLICATION_JSON_VALUE})
    public Line getLine(@PathVariable(value = "file") Integer file,@PathVariable(value = "line") Integer lineNumber){
        return LineControllerRepositoryMapper.lineRepositoryToLineController(
                lineService.getLineOfDocument(file, lineNumber));
    }

    @GetMapping(path = "/searchInFiles",produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<Line> searchLinesMatchingWord(@RequestParam(value ="fileList", required = true) List<Integer> fileList,
                              @RequestParam(value ="word", required = true) String word){
        return LineControllerRepositoryMapper.lineRepositoryToLineControllerAsArray(
                lineService.getAllLinesMatchWordInDocumentList(fileList, word));
    }

}
