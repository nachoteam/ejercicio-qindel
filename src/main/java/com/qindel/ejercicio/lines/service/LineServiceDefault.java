package com.qindel.ejercicio.lines.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.qindel.ejercicio.common.exceptions.JsonParsingException;
import com.qindel.ejercicio.common.exceptions.LineNotExistException;
import com.qindel.ejercicio.common.utils.JsonUtils;
import com.qindel.ejercicio.files.service.FileService;
import com.qindel.ejercicio.lines.models.repository.Line;
import com.qindel.ejercicio.lines.models.repository.LineIdentity;
import com.qindel.ejercicio.lines.repository.LineRepository;
import com.qindel.ejercicio.undo.models.repository.FileUndo;
import com.qindel.ejercicio.undo.repository.UndoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

@Service
public class LineServiceDefault implements LineService {

    private LineRepository lineRepository;
    private UndoRepository undoRepository;
    private FileService fileService;

    public LineServiceDefault (LineRepository lineRepository, UndoRepository undoRepository, FileService fileService){
        this.lineRepository = lineRepository;
        this.undoRepository = undoRepository;
        this.fileService  = fileService;
    }

    @Transactional
    public Integer insertLineInDocument(Line line){
        fileService.checkIfFileExist(line.getLineIdentity().getFileId());
        int lineCount = lineRepository.countByLineIdentityFileId(line.getLineIdentity().getFileId());
        Line savedLine;
        if(Objects.isNull(line.getLineIdentity().getLineNumber())||
                line.getLineIdentity().getLineNumber() > lineCount){

           savedLine = this.appendLineInDocument(line, lineCount);

        }else{
           lineRepository.updateLinesBeforeInsert(line.getLineIdentity().getFileId(),
                   line.getLineIdentity().getLineNumber());
            savedLine = lineRepository.save(line);

        }

        this.createBackupForUndo(savedLine, FileUndo.Action.DELETE, null);
        return savedLine.getLineIdentity().getLineNumber();
    }

    @Transactional
    public void deleteLineInDocument(Integer fileId, Integer lineNumber) {
        checkIfLineExist(fileId, lineNumber);

        LineIdentity identity = new LineIdentity(fileId, lineNumber);
        Line undoBackup = getUndoBackup(identity);
        try {
            this.createBackupForUndo(undoBackup, FileUndo.Action.INSERT, JsonUtils.getJsonStringFromObject(undoBackup));
        } catch (JsonProcessingException e) {
            throw new JsonParsingException("it seems that undo generation failed");
        }
        lineRepository.deleteById(identity);
        lineRepository.updateLinesAfterDelete(fileId, lineNumber);
    }

    private Line getUndoBackup(LineIdentity identity){
       Optional<Line> lineOptional = lineRepository.findById(identity);
       if(lineOptional.isPresent()){
           return lineOptional.get();
       }else{
           throw new LineNotExistException("this line doesnt exist in this file");
       }
    }

    @Transactional
    public void updateLineInDocument(Line line){
        checkIfLineExist(line.getLineIdentity().getFileId(), line.getLineIdentity().getLineNumber());

        Line undoBackup = getUndoBackup(line.getLineIdentity());

        try {
            this.createBackupForUndo(undoBackup, FileUndo.Action.MODIFY, JsonUtils.getJsonStringFromObject(undoBackup));
        } catch (JsonProcessingException e) {
            throw new JsonParsingException("it seems that undo generation failed");
        }

        lineRepository.save(line);
    }

    private Line appendLineInDocument(Line line, int position){
        line.getLineIdentity().setLineNumber(position + 1);
        return lineRepository.save(line);
    }

    public Line getLineOfDocument(Integer fileId, Integer lineNumber){
        checkIfLineExist(fileId, lineNumber);
        return lineRepository.findByLineIdentityFileIdAndLineIdentityLineNumber(fileId, lineNumber);
    }

    public List<Line> getAllLinesMatchWordInDocumentList(List<Integer> fileId, String pattern) {
        return lineRepository.findAllByLineIdentityFileIdInAndLineTextContains(fileId, pattern);
    }


    private void createBackupForUndo(Line savedLine, FileUndo.Action action, String data){
        Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());

        FileUndo undo = FileUndo.builder()
                .fileId(savedLine.getLineIdentity().getFileId())
                .lineNumber(savedLine.getLineIdentity().getLineNumber())
                .action(action).changeDate(currentTimestamp)
                .data(data).build();
        this.undoRepository.save(undo);

    }

    private void checkIfLineExist(Integer fileId, Integer lineNumber){
        fileService.checkIfFileExist(fileId);
        if(Objects.isNull(lineNumber) ||
                lineNumber > lineRepository.countByLineIdentityFileId(fileId)){
           throw new LineNotExistException("this line doesnt exist in this file");
        }
    }

}
