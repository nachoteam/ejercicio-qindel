package com.qindel.ejercicio.lines.service;

import com.qindel.ejercicio.lines.models.repository.Line;

import java.util.List;

public interface LineService {
    Integer insertLineInDocument(Line line);
    void updateLineInDocument(Line line);
    void deleteLineInDocument(Integer fileId, Integer lineNumber);

    Line getLineOfDocument(Integer fileId, Integer lineNumber);
    List<Line> getAllLinesMatchWordInDocumentList(List<Integer> fileId, String pattern);

}
