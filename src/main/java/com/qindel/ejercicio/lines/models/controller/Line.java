package com.qindel.ejercicio.lines.models.controller;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ApiModel(description="All data of line")
public class Line {
    @ApiModelProperty(notes = "File ID",name="fileId",required=true,value="1")
    @NotNull
    private Integer fileId;

    @ApiModelProperty(notes = "Line number",name="lineNumber",required=false)
    @Min(value = 1, message = "The value of lineNumber must be positive")
    private Integer lineNumber;

    @ApiModelProperty(notes = "Line data",name="lineText",required=true,value="line of the file")
    @NotNull
    private String lineText;
}
