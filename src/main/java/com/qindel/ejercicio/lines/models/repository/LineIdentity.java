package com.qindel.ejercicio.lines.models.repository;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LineIdentity implements Serializable {
    @NotNull
    @Column(name = "FILE_ID")
    private Integer fileId;

    @Column(name = "LINE_NUMBER")
    private Integer lineNumber;
}
