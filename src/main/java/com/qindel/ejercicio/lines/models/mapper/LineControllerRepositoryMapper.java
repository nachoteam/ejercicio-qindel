package com.qindel.ejercicio.lines.models.mapper;

import com.qindel.ejercicio.lines.models.controller.Line;
import com.qindel.ejercicio.lines.models.repository.LineIdentity;

import java.util.List;
import java.util.stream.Collectors;

public class LineControllerRepositoryMapper {

    private LineControllerRepositoryMapper(){}

    public static com.qindel.ejercicio.lines.models.repository.Line lineControllerToLineRepository(Line line) {
       return com.qindel.ejercicio.lines.models.repository.Line.builder()
                .lineIdentity(LineIdentity.builder().fileId(line.getFileId()).lineNumber(line.getLineNumber()).build())
                .lineText(line.getLineText()).build();
    }
    public static List<com.qindel.ejercicio.lines.models.repository.Line> lineControllerToLineRepositoryAsArray(List<Line> lineList) {
        return lineList.stream().map(line -> lineControllerToLineRepository(line)).collect(Collectors.toList());
    }

    public static Line lineRepositoryToLineController(com.qindel.ejercicio.lines.models.repository.Line line) {
        return Line.builder().fileId(line.getLineIdentity().getFileId())
                .lineNumber(line.getLineIdentity().getLineNumber()).lineText(line.getLineText()).build();
    }


    public static List<Line> lineRepositoryToLineControllerAsArray(List<com.qindel.ejercicio.lines.models.repository.Line> lineList) {
        return lineList.stream().map(line -> lineRepositoryToLineController(line)).collect(Collectors.toList());
    }
}
