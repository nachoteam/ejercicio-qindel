package com.qindel.ejercicio.lines.models.repository;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "LINE")
public class Line {

    @EmbeddedId
    private LineIdentity lineIdentity;

    @NotNull
    @Column(name = "LINE_VALUE")
    private String lineText;
}
