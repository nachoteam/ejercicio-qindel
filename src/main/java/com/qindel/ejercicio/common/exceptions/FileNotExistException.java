package com.qindel.ejercicio.common.exceptions;

public class FileNotExistException extends RuntimeException {
    private static final long serialVersionUID = -2281016954938630985L;

    public FileNotExistException(String message){
        super(message);
    }
}
