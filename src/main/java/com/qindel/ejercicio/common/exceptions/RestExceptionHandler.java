package com.qindel.ejercicio.common.exceptions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.qindel.ejercicio.common.utils.JsonUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.SQLException;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({FileNotExistException.class, LineNotExistException.class})
    protected ResponseEntity<Object> handleNotExistErrors(final RuntimeException ex, final WebRequest request) {
        try{
            return handleExceptionInternal(ex, JsonUtils.getJsonStringFromObject(ex.getMessage()), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
        }catch(JsonProcessingException e){
            return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
        }
    }

    @ExceptionHandler(SQLException.class)
    protected ResponseEntity<Object> handleSqlErrors(final RuntimeException ex, final WebRequest request) {
        try{
            return handleExceptionInternal(ex, JsonUtils.getJsonStringFromObject(ex.getMessage()), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
        }catch(JsonProcessingException e){
            return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
        }
    }

    @ExceptionHandler(JsonParsingException.class)
    protected ResponseEntity<Object> handleUndoErrors(final RuntimeException ex, final WebRequest request) {
        try{
            return handleExceptionInternal(ex, JsonUtils.getJsonStringFromObject(ex.getMessage()), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
        }catch(JsonProcessingException e){
            return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
        }
    }

    @ExceptionHandler(NotMoreUndoException.class)
    protected ResponseEntity<Object> handleNoMoreUndoErrors(final RuntimeException ex, final WebRequest request) {
        try{
            return handleExceptionInternal(ex, JsonUtils.getJsonStringFromObject(ex.getMessage()), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
        }catch(JsonProcessingException e){
            return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
        }
    }
}
