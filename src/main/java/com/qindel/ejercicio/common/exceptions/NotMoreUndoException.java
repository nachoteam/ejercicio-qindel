package com.qindel.ejercicio.common.exceptions;

public class NotMoreUndoException extends RuntimeException {

    private static final long serialVersionUID = -2281016954938630985L;

    public NotMoreUndoException(String message){
        super(message);
    }
}
