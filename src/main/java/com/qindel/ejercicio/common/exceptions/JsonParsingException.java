package com.qindel.ejercicio.common.exceptions;

public class JsonParsingException extends RuntimeException{
    private static final long serialVersionUID = -2281016954938630985L;

    public JsonParsingException(String message){
        super(message);
    }
}
