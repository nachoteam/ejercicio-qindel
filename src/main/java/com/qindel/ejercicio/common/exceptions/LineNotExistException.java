package com.qindel.ejercicio.common.exceptions;

public class LineNotExistException extends RuntimeException {

    private static final long serialVersionUID = -2281016954938630985L;

    public LineNotExistException(String message){
        super(message);
    }

}
