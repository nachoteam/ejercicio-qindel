package com.qindel.ejercicio.common.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class JsonUtils {

    private static ObjectMapper	objectMapper = new ObjectMapper();

    private JsonUtils() {
    }

    public static String getJsonStringFromObject(final Object obj) throws JsonProcessingException {
            return objectMapper.writeValueAsString(obj);
    }

    public static <T> T getObjectFromJsonString(Class<T> clazz, String requestJson) throws IOException {
        T t = null;
        InputStream is = new ByteArrayInputStream(requestJson.getBytes("UTF-8"));
        t = objectMapper.readValue(is, clazz);
        return t;
    }

}