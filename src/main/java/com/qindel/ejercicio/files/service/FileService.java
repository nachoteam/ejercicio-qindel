package com.qindel.ejercicio.files.service;

import com.qindel.ejercicio.files.models.repository.FileQ;
import com.qindel.ejercicio.lines.models.repository.Line;
import org.springframework.core.io.Resource;

import java.util.List;

public interface FileService {
    Integer createFile(FileQ file);
    List<FileQ> listAllFiles();

    Resource getAllLinesAsResource(Integer file);

    int countLinesInDocument(Integer fileId);
    List<Line> getAllLinesOfDocument(Integer fileId);
    void checkIfFileExist(Integer fileId);
}
