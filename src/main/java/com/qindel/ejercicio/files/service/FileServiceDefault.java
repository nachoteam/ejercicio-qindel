package com.qindel.ejercicio.files.service;

import com.qindel.ejercicio.common.exceptions.FileNotExistException;
import com.qindel.ejercicio.files.models.repository.FileQ;
import com.qindel.ejercicio.files.repository.FileRepository;
import com.qindel.ejercicio.lines.models.repository.Line;
import com.qindel.ejercicio.lines.repository.LineRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class FileServiceDefault implements FileService {

    private FileRepository fileRepository;
    private LineRepository lineRepository;
    private static final String END_OF_LINE = "\n";

    public FileServiceDefault(FileRepository fileRepository, LineRepository lineRepository){
        this.fileRepository = fileRepository;
        this.lineRepository = lineRepository;
    }


    public Integer createFile(FileQ file){
        return fileRepository.save(file).getFileId();
    }

    public List<FileQ> listAllFiles(){
         return StreamSupport.stream( fileRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public Resource getAllLinesAsResource(Integer file) {
        this.checkIfFileExist(file);

        List<Line> allLines = this.getAllLinesOfDocument(file);
        List<String> allLinesAsString = allLines.stream().map(Line::getLineText).collect(Collectors.toList());
        InputStream data = new ByteArrayInputStream( StringUtils.join(allLinesAsString, END_OF_LINE).getBytes() );
        return new InputStreamResource(data);
    }

    public void checkIfFileExist(Integer fileId){
        if(!fileRepository.findById(fileId).isPresent()){
            throw new FileNotExistException("this file doesnt exist");
        }
    }

    public int countLinesInDocument(Integer fileId){
        checkIfFileExist(fileId);
        return lineRepository.countByLineIdentityFileId(fileId);
    }


    public List<Line> getAllLinesOfDocument(Integer fileId){
        checkIfFileExist(fileId);

        return lineRepository.findByLineIdentityFileIdOrderByLineIdentityLineNumberAsc(fileId);
    }

}
