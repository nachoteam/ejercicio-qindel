package com.qindel.ejercicio.files.repository;

import com.qindel.ejercicio.files.models.repository.FileQ;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends CrudRepository <FileQ, Integer> {
}
