package com.qindel.ejercicio.files.controller;


import com.qindel.ejercicio.files.models.controller.FileQ;
import com.qindel.ejercicio.files.models.mapper.FileControllerRepositoryMapper;
import com.qindel.ejercicio.files.service.FileService;
import com.qindel.ejercicio.lines.models.controller.Line;
import com.qindel.ejercicio.lines.models.mapper.LineControllerRepositoryMapper;
import com.qindel.ejercicio.undo.service.UndoService;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/file")
public class FileController implements FileApiDocumentation {

    private FileService fileService;
    private UndoService undoService;

    public FileController(FileService fileService, UndoService undoService){
        this.fileService = fileService;
        this.undoService = undoService;
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public Integer createNewFile(@Valid @NotNull @RequestBody FileQ file){

        return fileService.createFile(FileControllerRepositoryMapper.fileControllerToFileRepository(file));
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<FileQ> listAllFiles(){
        return FileControllerRepositoryMapper.fileRepositoryToFileControllerAsArray(fileService.listAllFiles());
    }


    // GET METHOD
    @GetMapping(path = "/{file}/countLines",produces = {MediaType.APPLICATION_JSON_VALUE})
    public Integer countLinesOfFile(@PathVariable(value = "file") Integer file){
        return fileService.countLinesInDocument(file);
    }

    // ALL FILE GET METHODS
    @GetMapping(path = "/{file}",produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<Line> getAllLines(@PathVariable(value = "file") Integer file){
        return LineControllerRepositoryMapper.lineRepositoryToLineControllerAsArray(
                fileService.getAllLinesOfDocument(file));
    }

    @GetMapping(path = "/{file}/allFileAsTxtFile",produces = {"Text/txt"})
    public Resource getFileAsTxt(@PathVariable(value = "file") Integer file){
        return fileService.getAllLinesAsResource(file);
    }

    // Dado que en el undo puede haber operaciones tanto de insercion, actualizacion y borrado
    // no se esta seguro de que metodo se deberia seguir
    // dado que desde un enfoque orientado a documento, se podria considerar una actualizacion,
    // se ha decidido que se aplique el metodo PUT.
    // por otro lado, como lo que se deshace es el ultimo movimiento, no el que elija el usuario
    // no se considera necesario recibir ningun parametro
    @PutMapping(path = "{file}/undo")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void undoActionOverDocument(@PathVariable(value = "file") Integer fileId){
        this.undoService.undoLastChange(fileId);
    }

}
