package com.qindel.ejercicio.files.controller;

import com.qindel.ejercicio.files.models.controller.FileQ;
import com.qindel.ejercicio.lines.models.controller.Line;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.core.io.Resource;

import java.io.File;
import java.util.List;

public interface FileApiDocumentation  {

    @ApiOperation(value = "Qindel File", notes = "Endpoint to create a new file", tags="edit file")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Success|OK"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "internal server error") })
    Integer createNewFile(FileQ file);

    @ApiOperation(value = "None", notes = "Endpoint undo last change over a file", tags="edit file")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No content"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "internal server error") })
    void undoActionOverDocument(Integer fileId);


    @ApiOperation(value = "All files", notes = "Endpoint to list all files in the sistem", response = List.class, tags="info files")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK", response = List.class),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "internal server error") })
    List<FileQ> listAllFiles();

    @ApiOperation(httpMethod = "GET", value = "Lines", notes = "Endpoint to get all lines of a file", response = List.class, tags="info files")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK", response = List.class),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "internal server error") })
    List<Line> getAllLines( Integer file);

    @ApiOperation(httpMethod = "GET", value = "Plain text", notes = "Endpoint to get all lines of a file", response = File.class, tags="info files")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK", response = List.class),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "internal server error") })
    Resource getFileAsTxt(Integer file);

    @ApiOperation(httpMethod = "GET", value = "Integer", notes = "Endpoint to count lines of a file", response = Integer.class, tags="info files")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK", response = Integer.class),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "internal server error") })
    Integer countLinesOfFile(Integer file);

}
