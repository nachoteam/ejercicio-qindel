package com.qindel.ejercicio.files.models.repository;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "FILE")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FileQ {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FILE_ID")
    private Integer fileId;
    @Column(name = "FILE_NAME")
    private String name;
}
