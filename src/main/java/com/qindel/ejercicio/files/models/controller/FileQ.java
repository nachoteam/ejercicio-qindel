package com.qindel.ejercicio.files.models.controller;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ApiModel(description="All data of file")
public class FileQ {

    //@Null
    private Integer fileId;
    @ApiModelProperty(notes = "File name",name="name",required=true,value="Harry Potter")
    @NotNull
    private String name;
}
