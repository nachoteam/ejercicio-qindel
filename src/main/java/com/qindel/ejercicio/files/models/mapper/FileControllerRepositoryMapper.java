package com.qindel.ejercicio.files.models.mapper;

import com.qindel.ejercicio.files.models.controller.FileQ;

import java.util.List;
import java.util.stream.Collectors;

public class FileControllerRepositoryMapper {
    private FileControllerRepositoryMapper(){}

    public static com.qindel.ejercicio.files.models.repository.FileQ fileControllerToFileRepository(FileQ file) {
        return  com.qindel.ejercicio.files.models.repository.FileQ.builder()
                .fileId(file.getFileId())
                .name(file.getName())
                .build();
    }

    public static List<com.qindel.ejercicio.files.models.repository.FileQ> fileControllerToFileRepositoryAsArray(List<FileQ> fileList) {
        return fileList.stream().map(file -> fileControllerToFileRepository(file)).collect(Collectors.toList());
    }

    public static FileQ fileRepositoryToFileController(com.qindel.ejercicio.files.models.repository.FileQ file) {
        return  FileQ.builder()
                .fileId(file.getFileId())
                .name(file.getName())
                .build();
    }

    public static List<FileQ> fileRepositoryToFileControllerAsArray(List<com.qindel.ejercicio.files.models.repository.FileQ> fileList) {
        return fileList.stream().map(file -> fileRepositoryToFileController(file)).collect(Collectors.toList());
    }
}
