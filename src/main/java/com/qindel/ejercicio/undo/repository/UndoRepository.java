package com.qindel.ejercicio.undo.repository;

import com.qindel.ejercicio.undo.models.repository.FileUndo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UndoRepository extends JpaRepository<FileUndo, Integer> {
    FileUndo  findFirstByFileIdOrderByChangeDateDesc(Integer fileId);
}
