package com.qindel.ejercicio.undo.service;

import com.qindel.ejercicio.common.exceptions.JsonParsingException;
import com.qindel.ejercicio.common.exceptions.NotMoreUndoException;
import com.qindel.ejercicio.common.utils.JsonUtils;
import com.qindel.ejercicio.lines.models.repository.Line;
import com.qindel.ejercicio.lines.service.LineService;
import com.qindel.ejercicio.undo.models.repository.FileUndo;
import com.qindel.ejercicio.undo.repository.UndoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Objects;

@Service
public class UndoServiceDefault implements UndoService {

    private UndoRepository undoRepository;
    private LineService lineService;

    public UndoServiceDefault(UndoRepository undoRepository, LineService lineService){
        this.undoRepository = undoRepository;
        this.lineService = lineService;
    }


    @Transactional
    public void undoLastChange(Integer fileId) {
        FileUndo undo = this.undoRepository.findFirstByFileIdOrderByChangeDateDesc(fileId);
        if(Objects.isNull(undo)){
            throw new NotMoreUndoException("Not more undo actions in database for this file or file does not exist");
        }

        try {
            if (undo.getAction() == FileUndo.Action.INSERT) {
                lineService.insertLineInDocument(JsonUtils.getObjectFromJsonString(Line.class, undo.getData()));
            } else if (undo.getAction() == FileUndo.Action.DELETE) {
                lineService.deleteLineInDocument(undo.getFileId(), undo.getLineNumber());
            }else if (undo.getAction() == FileUndo.Action.MODIFY) {
                lineService.updateLineInDocument(JsonUtils.getObjectFromJsonString(Line.class, undo.getData()));
            }
        }catch(IOException e){
            throw new JsonParsingException("problem parsing data, undo cant be done");
        }
        undoRepository.deleteById(undo.getChangeId());
        FileUndo undoChangeSave = this.undoRepository.findFirstByFileIdOrderByChangeDateDesc(fileId);
        undoRepository.deleteById(undoChangeSave.getChangeId());
    }

}
