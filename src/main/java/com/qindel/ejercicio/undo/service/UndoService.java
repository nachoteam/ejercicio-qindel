package com.qindel.ejercicio.undo.service;

public interface UndoService {
    void undoLastChange(Integer fileId);
}
