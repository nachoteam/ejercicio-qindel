package com.qindel.ejercicio.undo.models.repository;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "CHANGE_TRACKING")
public class FileUndo {

    public enum Action {INSERT, DELETE, MODIFY}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CHANGE_ID")
    private Integer changeId;

    @NotNull
    @Column(name = "FILE_ID")
    private Integer fileId;

    @NotNull
    @Column(name = "LINE_NUM")
    private Integer lineNumber;

    @NotNull
    @Column(name = "ACTION")
    private Action action;


    @Column(name = "DATA_RECOVERY")
    private String data;

    @NotNull
    @Column(name = "DATE_CHANGE")
    private Timestamp changeDate;
}
