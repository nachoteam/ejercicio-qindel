DROP TABLE IF EXISTS LINE;
DROP TABLE IF EXISTS FILE;

create table FILE (
 FILE_ID integer not null auto_increment,
 FILE_NAME varchar not null,
  primary key (FILE_ID)
);

create table LINE (
 FILE_ID integer not null,
 LINE_NUMBER integer not null,
 LINE_VALUE varchar,
 primary key (LINE_NUMBER),
 foreign key (FILE_ID) references FILE(FILE_ID)
);

create table CHANGE_TRACKING(
  CHANGE_ID integer not null auto_increment,
  FILE_ID integer not null,
  LINE_NUM integer not null,
  DATE_CHANGE TIMESTAMP not null,
  ACTION varchar not null,
  DATA_RECOVERY VARCHAR,
  primary key (CHANGE_ID),
  foreign key (FILE_ID) references FILE(FILE_ID)
);
