package com.qindel.ejercicio.lines.controller;

import com.qindel.ejercicio.common.exceptions.FileNotExistException;
import com.qindel.ejercicio.common.exceptions.LineNotExistException;
import com.qindel.ejercicio.common.exceptions.RestExceptionHandler;
import com.qindel.ejercicio.common.utils.JsonUtils;
import com.qindel.ejercicio.lines.models.controller.Line;
import com.qindel.ejercicio.lines.models.repository.LineIdentity;
import com.qindel.ejercicio.lines.service.LineServiceDefault;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(MockitoJUnitRunner.class)
public class LineControllerDefaultTest {

    @Mock
    private LineServiceDefault lineService;
    @InjectMocks
    private LineController lineController;

    private MockMvc mockMvc;

    @Before
    public void setup() {
         this.mockMvc = MockMvcBuilders.standaloneSetup(lineController).setControllerAdvice(new RestExceptionHandler()).build();
    }

    private com.qindel.ejercicio.lines.models.repository.Line createLine(Integer fileId, Integer lineNumber, String lineText){
        return com.qindel.ejercicio.lines.models.repository.Line.builder()
                .lineIdentity(LineIdentity.builder().fileId(fileId).lineNumber(lineNumber).build())
                .lineText(lineText).build();
    }

    @Test
    public void getLineOk() throws Exception{
        //given
        com.qindel.ejercicio.lines.models.repository.Line line = createLine(1,3, "Erase una vez");

        //when
        Mockito.when(lineService.getLineOfDocument(any(), any())).thenReturn(line);
        mockMvc.perform(get("/line/{file}/{line}","2", "3" ))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
    }

    @Test
    public void getLineFileNotExistKO() throws Exception{
        //given
        com.qindel.ejercicio.lines.models.repository.Line line = createLine(1,3, "Erase una vez");
        //when
        Mockito.when(lineService.getLineOfDocument(any(), any())).thenThrow(new FileNotExistException("file not exist"));
        mockMvc.perform(get("/line/{file}/{line}","2", "3" ))
                .andExpect(MockMvcResultMatchers.status().isNotFound()).andReturn();
    }

    @Test
    public void getLineNotExistKO() throws Exception{
        //given
        com.qindel.ejercicio.lines.models.repository.Line line = createLine(1,3, "Erase una vez");
        //when
        Mockito.when(lineService.getLineOfDocument(any(), any())).thenThrow(new LineNotExistException("line not exist"));
        mockMvc.perform(get("/line/{file}/{line}","2", "3" ))
                .andExpect(MockMvcResultMatchers.status().isNotFound()).andReturn();
    }


    @Test
    public void postInsertLineOk() throws Exception{
        //given
        Line line = new Line(1,3, "Erase una vez");

        //when

        Mockito.when(lineService.insertLineInDocument(any())).thenReturn(3);

     //   Mockito.doNothing().when(lineService).insertLineInDocument(any());
        mockMvc.perform(post("/line/create")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(JsonUtils.getJsonStringFromObject(line)))
                .andExpect(MockMvcResultMatchers.status().isCreated()).andReturn();
    }

    @Test
    public void putModifyLineOk() throws Exception{
        //given
        Line line = new Line(1,3, "Erase una vez");

        //when
        Mockito.doNothing().when(lineService).updateLineInDocument(any());

        mockMvc.perform(put("/line/modify")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(JsonUtils.getJsonStringFromObject(line)))
                .andExpect(MockMvcResultMatchers.status().isNoContent()).andReturn();
    }

    @Test
    public void putModifyLineBadRequestKO() throws Exception{
        //given
        Line line = new Line(null, null, null);

        //when
        mockMvc.perform(put("/line/modify")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(JsonUtils.getJsonStringFromObject(line)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
    }


    @Test
    public void deleteLineOk() throws Exception{
        //given
        //when
        Mockito.doNothing().when(lineService).deleteLineInDocument(any(), any());
        mockMvc.perform(delete("/line/{file}/{line}","2", "3" )
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent()).andReturn();
    }

}
