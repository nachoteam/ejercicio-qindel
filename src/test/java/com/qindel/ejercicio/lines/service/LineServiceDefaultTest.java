package com.qindel.ejercicio.lines.service;

import com.qindel.ejercicio.common.exceptions.FileNotExistException;
import com.qindel.ejercicio.common.exceptions.LineNotExistException;
import com.qindel.ejercicio.files.models.repository.FileQ;
import com.qindel.ejercicio.files.service.FileService;
import com.qindel.ejercicio.lines.models.repository.Line;
import com.qindel.ejercicio.lines.models.repository.LineIdentity;
import com.qindel.ejercicio.lines.repository.LineRepository;
import com.qindel.ejercicio.undo.repository.UndoRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Optional;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LineServiceDefaultTest {


    @Mock
    private LineRepository lineRepository;

    @Mock
    private FileService fileService;

    @Mock
    private UndoRepository undoRepository;

    @InjectMocks
    private LineServiceDefault lineService;

    private Line createLine(Integer fileId, Integer lineNumber, String lineText){
       return Line.builder()
               .lineIdentity(LineIdentity.builder().fileId(fileId).lineNumber(lineNumber).build())
               .lineText(lineText).build();
    }

    @Test
    public void insertLineOk(){
        //given
        Line line = createLine(1,3, "Erase una vez");

        //when
        Mockito.doNothing().when(fileService).checkIfFileExist(any());
        Mockito.when(lineRepository.countByLineIdentityFileId(any())).thenReturn(4);
        Mockito.doNothing().when(lineRepository).updateLinesBeforeInsert(any(), any());
        Mockito.when(lineRepository.save(any())).thenReturn(line);

        Mockito.when(undoRepository.save(any())).thenReturn(null);

        lineService.insertLineInDocument(line);

        //then
        verify(fileService).checkIfFileExist(any());
        verify(lineRepository).countByLineIdentityFileId(any());
        verify(lineRepository).save(any());
        verify(lineRepository).updateLinesBeforeInsert(any(),any());
    }

    @Test
    public void appendLineOk(){
        //given
        Line line = createLine(1,8, "Erase una vez");
        //when
        Mockito.doNothing().when(fileService).checkIfFileExist(any());
        Mockito.when(lineRepository.countByLineIdentityFileId(any())).thenReturn(4);
        Mockito.when(lineRepository.save(any())).thenReturn(line);

        lineService.insertLineInDocument(line);

        //then
        verify(fileService).checkIfFileExist(any());
        verify(lineRepository).countByLineIdentityFileId(any());
        verify(lineRepository).save(any());
    }

    @Test(expected = FileNotExistException.class)
    public void insertLineFileNotExistKO(){
        //given
        Line line = createLine(1,8, "Erase una vez");
        //when
        Mockito.doThrow(new FileNotExistException("file not exist")).when(fileService).checkIfFileExist(any());
        lineService.insertLineInDocument(line);
        //then
        verify(fileService).checkIfFileExist(any());
    }


    @Test
    public void updateLineOk(){
        //given
        Line line = createLine(1,3, "Erase una vez");
        Optional<Line> optLine = Optional.of(line);
        Optional<FileQ> checkFileMock = Optional.of(new FileQ(5,"Harry Potter"));

        //when
        Mockito.doNothing().when(fileService).checkIfFileExist(any());
        Mockito.when(lineRepository.countByLineIdentityFileId(any())).thenReturn(4);
        Mockito.when(lineRepository.save(any())).thenReturn(line);// do nothing

        Mockito.when(lineRepository.findById(any())).thenReturn(optLine);
        Mockito.when(undoRepository.save(any())).thenReturn(null);


        lineService.updateLineInDocument(line);

        //then
        verify(fileService).checkIfFileExist(any());
        verify(lineRepository).countByLineIdentityFileId(any());
        verify(lineRepository).save(any());
    }

    @Test(expected = FileNotExistException.class)
    public void updateLineFileNotExistKO(){
        //given
        Optional<FileQ> checkFileMock = Optional.empty();
        //when
        Mockito.doThrow(new FileNotExistException("file not exist")).when(fileService).checkIfFileExist(any());
        lineService.deleteLineInDocument(5,5);
        //then
        verify(fileService).checkIfFileExist(any());
    }

    @Test(expected = LineNotExistException.class)
    public void updateLineNotExistKO(){
        //given
        Line line = createLine(1,7, "Erase una vez");

        Optional<FileQ> checkFileMock = Optional.of(new FileQ(5,"harry Potter")); //optional valido, archivo existe
        //when
        Mockito.doNothing().when(fileService).checkIfFileExist(any());
        Mockito.when(lineRepository.countByLineIdentityFileId(any())).thenReturn(4);

        lineService.updateLineInDocument(line);

        //then
        verify(fileService).checkIfFileExist(any());
        verify(lineRepository).countByLineIdentityFileId(any());
    }


    @Test
    public void deleteLineOk(){
        //given
        Line line = createLine(1,3, "Erase una vez");
        Optional<FileQ> checkFileMock = Optional.of(new FileQ(5,"Harry Potter"));
        Optional<Line> optLine = Optional.of(line);

        //when
        Mockito.doNothing().when(fileService).checkIfFileExist(any());
        Mockito.when(lineRepository.countByLineIdentityFileId(any())).thenReturn(4);
        Mockito.doNothing().when(lineRepository).updateLinesAfterDelete(any(), any());
        Mockito.doNothing().when(lineRepository).deleteById(any());

        Mockito.when(lineRepository.findById(any())).thenReturn(optLine);
        Mockito.when(undoRepository.save(any())).thenReturn(null);

        lineService.deleteLineInDocument(5,2);

        //then
        verify(fileService).checkIfFileExist(any());
        verify(lineRepository).countByLineIdentityFileId(any());
        verify(lineRepository).deleteById(any());
        verify(lineRepository).updateLinesAfterDelete(any(),any());

    }

    @Test(expected = FileNotExistException.class)
    public void deleteLineFileNotExistKO(){
        //given

        //then
        Mockito.doThrow(new FileNotExistException("file not exist")).when(fileService).checkIfFileExist(any());

        lineService.deleteLineInDocument(5,5);
        //when
        verify(fileService).checkIfFileExist(any());
    }

    @Test(expected = LineNotExistException.class)
    public void deleteLineNotExistKO(){
        //given
        Optional<FileQ> checkFileMock = Optional.of(new FileQ(5,"harry Potter")); //optional valido, archivo existe
        //when
        Mockito.doNothing().when(fileService).checkIfFileExist(any());
        Mockito.when(lineRepository.countByLineIdentityFileId(any())).thenReturn(4);

        lineService.deleteLineInDocument(5,6);
        //then
        verify(fileService).checkIfFileExist(any());
        verify(lineRepository).countByLineIdentityFileId(any());
    }

    @Test
    public void getAllMatchingWordsInDocumentListOK(){
        //given
        ArrayList<Integer> files = new ArrayList<Integer>();
        files.add(3);
        files.add(4);
        String word = "the";

        ArrayList<Line> lines = new ArrayList<Line>();
        lines.add(createLine(1,3, "Erase una vez"));
        //then
        Mockito.when(lineRepository.findAllByLineIdentityFileIdInAndLineTextContains(any(), any())).thenReturn(lines);
        lineService.getAllLinesMatchWordInDocumentList(files, word);
        //when
        verify(lineRepository).findAllByLineIdentityFileIdInAndLineTextContains(any(), any());
    }


    @Test
    public void getLineOfDocumentOK(){
        //given
        Line lineExpected = createLine(5,3, "Erase una vez");
        Optional<FileQ> checkFileMock = Optional.of(new FileQ(5,"harry Potter")); //optional valido, archivo existe

        //when
        Mockito.doNothing().when(fileService).checkIfFileExist(any());
        Mockito.when(lineRepository.countByLineIdentityFileId(any())).thenReturn(4);
        Mockito.when(lineRepository.findByLineIdentityFileIdAndLineIdentityLineNumber(any(), any())).thenReturn(lineExpected);

        Line line = lineService.getLineOfDocument(5, 3);

        //then
        verify(fileService).checkIfFileExist(any());
        verify(lineRepository).countByLineIdentityFileId(any());
        verify(lineRepository).findByLineIdentityFileIdAndLineIdentityLineNumber(any(), any());

        Assert.assertEquals(lineExpected.getLineText(), line.getLineText());
    }

}

