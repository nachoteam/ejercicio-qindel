package com.qindel.ejercicio.files.service;

import com.qindel.ejercicio.common.exceptions.FileNotExistException;
import com.qindel.ejercicio.files.models.repository.FileQ;
import com.qindel.ejercicio.files.repository.FileRepository;
import com.qindel.ejercicio.lines.models.repository.Line;
import com.qindel.ejercicio.lines.models.repository.LineIdentity;
import com.qindel.ejercicio.lines.repository.LineRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class FileServiceDefaultTest {

    @Mock
    private FileRepository fileRepository;
    @Mock
    private LineRepository lineRepository;

    @InjectMocks
    private FileServiceDefault fileService;


    private Line createLine(Integer fileId, Integer lineNumber, String lineText){
        return Line.builder()
                .lineIdentity(LineIdentity.builder().fileId(fileId).lineNumber(lineNumber).build())
                .lineText(lineText).build();
    }

    @Test
    public void getAllLinesOfDocumentOK(){
        //given
        ArrayList<Line> expectedLines = new ArrayList<Line>();
        expectedLines.add(createLine(5,3, "Erase una vez"));

        Optional<FileQ> checkFileMock = Optional.of(new FileQ(5,"harry Potter")); //optional valido, archivo existe
        //when
        Mockito.when(fileRepository.findById(any())).thenReturn(checkFileMock);
        Mockito.when(lineRepository.findByLineIdentityFileIdOrderByLineIdentityLineNumberAsc(any())).thenReturn(expectedLines);

        List<Line> lines = fileService.getAllLinesOfDocument(5);
        //then
        verify(fileRepository).findById(any());
        verify(lineRepository).findByLineIdentityFileIdOrderByLineIdentityLineNumberAsc(any());
        Assert.assertEquals(expectedLines.size(), lines.size());
    }



    @Test
    public void countLinesInDocumentOK(){
        //given
        int expectedLines = 15;
        Optional<FileQ> checkFileMock = Optional.of(new FileQ(5,"harry Potter")); //optional valido, archivo existe

        //when
        Mockito.when(fileRepository.findById(any())).thenReturn(checkFileMock);
        Mockito.when(lineRepository.countByLineIdentityFileId(any())).thenReturn(expectedLines);

        int linesCount = fileService.countLinesInDocument(5);
        //then
        verify(fileRepository).findById(any());
        verify(lineRepository).countByLineIdentityFileId(any());

        Assert.assertEquals(expectedLines, linesCount);
    }


    @Test(expected = FileNotExistException.class)
    public void countLinesInDocumentNotExistKO(){
        //given
        Optional<FileQ> checkFileMock = Optional.empty();
        //when
        Mockito.when(fileRepository.findById(any())).thenReturn(checkFileMock);

        fileService.countLinesInDocument(5);
        //then
        verify(fileRepository).findById(any());
    }


}
