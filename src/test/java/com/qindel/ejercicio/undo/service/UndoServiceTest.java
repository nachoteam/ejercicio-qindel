package com.qindel.ejercicio.undo.service;

import com.qindel.ejercicio.common.exceptions.NotMoreUndoException;
import com.qindel.ejercicio.lines.service.LineService;
import com.qindel.ejercicio.undo.models.repository.FileUndo;
import com.qindel.ejercicio.undo.repository.UndoRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UndoServiceTest {

    @Mock
    private UndoRepository undoRepository;
    @Mock
    private LineService lineService;

    @InjectMocks
    private UndoServiceDefault undoService;

    @Test
    public void undoChangeOk(){
        //given
        FileUndo lastChange= FileUndo.builder()
                .changeId(1)
                .fileId(1)
                .lineNumber(2)
                .action(FileUndo.Action.DELETE)
                .data(null)
                .changeDate(null)
                .build();
        //when
        Mockito.when(undoRepository.findFirstByFileIdOrderByChangeDateDesc(any())).thenReturn(lastChange);
        Mockito.doNothing().when(lineService).deleteLineInDocument(any(), any());
        Mockito.doNothing().when(undoRepository).deleteById(any());

        undoService.undoLastChange(5);

        //then
        verify(undoRepository, times(2)).findFirstByFileIdOrderByChangeDateDesc(any());
        verify(lineService).deleteLineInDocument(any(), any());
        verify(undoRepository,times(2)).deleteById(any());
    }

    @Test(expected = NotMoreUndoException.class)
    public void undoChangeNoChangeKO(){
        //given
        FileUndo lastChange= null;
        // when
        Mockito.when(undoRepository.findFirstByFileIdOrderByChangeDateDesc(any())).thenReturn(lastChange);
        //then
        undoService.undoLastChange(5);
    }

}
